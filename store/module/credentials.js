export default {
    state: () => (
        {
            token: "",
            roleList: "",
            username: "",
        }
    ),
    actions: {
        setCredentials({ commit }, credentials) {
            commit('SET_TOKEN', credentials.token)
            commit('SET_ROLE', credentials.roleList)
            commit('SET_USER', credentials.username, credentials.password)
        },
    },
    mutations: {
        SET_TOKEN (state, token) {
            state.token = token
        },
        SET_ROLE (state, roleList) {
            state.roleList = roleList
        },
        SET_USER (state, username, password) {
            state.username = username
            state.password = password
        },
        CLEAR_CREDENTIALS(state) {
            state = {
                    token: "",
                    roleList: "",
                    username: ""

            }
        }
    },
    getters: {
        getToken (state) {
            return state.token
        },
        getUsername (state) {
            return state.username
        }
    }
}
