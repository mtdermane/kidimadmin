/* eslint-disable prettier/prettier */
import Vue from 'vue'
import Vuex from 'vuex'
import credentials from './module/credentials'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    credentials
  },
  plugins: [createPersistedState()]
})

export default store
